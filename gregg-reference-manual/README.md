# The Gregg Reference Manual (Annotations)

The *Gregg Reference Manual* has been the American English standard for business writing style, grammar, usage and formatting for over half a century. "The Bible" (as it's commonly known) is even more relevant now since it fits well with online writing in articles, blogs, and more. The latest edition is the [11th Tribute Edition](https://www.amazon.com/Gregg-Reference-Manual-11th-eleventh/dp/B005GIHKYK).

## Frequently Asked Questions

Here's a place to put examples that are commonly looked up.

Bulleted Lists:

* Full sentence gets a period.
* Everything gets capitalized.
* Long terms are bold with period.

* Some
* Thing

* **HTTP (Hypertext Transfer Protocol).** The most widely used of all the protocols, HTTP permits you to surf the WorldWideWeb---that part of the Internet that provides access not only to text material but also to images, animation, video and audio.
* **Gopher.** Gopher permits you to access text plus graphical and audio materials, but it first provides you with a series of menus that become progressively more specific until you locate the information you are looking for.


