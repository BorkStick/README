---
Title: Continuous Integration / Continuous Delivery
Subtitle: CI/CD
---

CI/CD refers to the combined practices of continuous integration and continuous delivery. By automating the quality assurance testing changes can be made instantly and released on a [rotational](/rotating-release/) schedule allowing them to be usable sooner. This concept is popular in the technology industry but has made its way into other fast-moving industries as well, even knowledge management and mentored learning systems (such as this [knowledge app](/pka/)).
