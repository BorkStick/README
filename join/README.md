---
Title: Join the Community
---

Currently the community meets remotely online, primarily live through [Twitch](/twitch/) (and [Twitch IRC](/irc/)), but also on [Discord](/discord/). [YouTube](https://www.youtube.com/c/rwxrob) is simply a place to store past videos (which are delayed until 24 hours after creation due to Twitch terms of service).

---------- -------------------------------------------------
 Twitch    <https://rwxrob.live>  
 Discord   <https://https://discordapp.com/invite/7s35TRB>  
 YouTube   <https://www.youtube.com/c/rwxrob>
---------- ------------------------------------------------

## Cost to Join

There is no cost to join the community (and there never will be). But [donations](/donate/) and [contributions](/contribute/) keep the community alive and progressing.


