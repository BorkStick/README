---
Title: Goals
Subtitle: "What will I get?"
---

The goals for this course are rather simple. If everyone works together and you put in the time here's what you will get:

1. Empowerment and ownership of your learning, forever
1. Mastery of beginner coding and Linux skills
1. Proof

That's really it. 

