---
Title: Community and Course Rules
---

Be nice. Be understanding. Be helpful. Be patient. You aren't competing against anyone but yourself. So don't feel like you have to horde your knowledge to do better than the others. There's no "bonus" and no fucking bell curve. You *and* we, a learning community bound together with one purpose, to learn and help each other learn. So forget your tricks to get the A, your cram sessions just to pass a test and forget immediately, your multiple-guess test taking skills, your alternative agenda, and your scarcity mentality. Throw them all in the trash bin of history where then belong. Bring nothing but empathy, authenticity, and personal commitment you'll do just fine. Don't forget. There's no "failing grade" here, just progress at your pace, on your terms.

One thing you should know, however, we are all going to give you a good-natured hard time for one specific thing: intellectual laziness. We might joke asking things like, "Humm, I wonder if you can find the answer to your current questions by looking harder at the current screen?" This is for your benefit. People in the industry will *not* be as good-natured. We're just nudging you in the right direction, getting you to [answer your own questions](/autodidact/).

But, if anyone seriously struggles with something about which they have done a lot of research and just cannot figure out, and they actually have the courage to ask publicly for help, and *you* actually have the gall to make them feel small or stupid to stroke your own ego, well, you won't just get banned, we will find you and take turns personally kicking your ass. So just don't.

