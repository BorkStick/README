---
Title: Minimum Viable Knowledge Base
Subtitle: Consumable, Composable, Sustainable, Shareable
---

A *minimum viable knowledge base* is a [knowledge base](/knowledge-base/) that follows the "do one thing well" philosophy. While the MVKB might contain thousands of knowledge nodes the base itself makes sense as a single entity which itself can be composed into other knowledge bases. *Minimum viable* refers to the simplest possible tech requirement possible, which almost always means some form of Markdown in a Git repo, a [README](/readme/) that can be shared with the [world](https://readme.world/).

