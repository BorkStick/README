---
Title: Linux Beginner Boost
NoTitle: true
NoTitleHead: true
Version: 0.0.6
---

<div class=boxpoint>
[**Welcome to <span class=spy>RWX.GG</span>,**]{.biggest}  
<span class=bigger>the [knowledge app](/pka/) for  
*Linux Beginner Boost*.

<span class=big>*BBoost* is a [flipped](/flipped/) and [intense](/schedule/) [open course](/open-course/) and [credential](/open-credential/). Chances are you haven't experienced anything like it in traditional education. You'll need more than just this app to get you through it. But don't worry. [Join the community.](/join/)  
We got you.

[**So what'll you learn?**]{.bigger}  
First you'll learn how to set yourself up to be an effective, tech-focused, [autodidact](/autodidact/). (If you just looked that up you probably already are one.) After learning how to learn you'll master the Linux command line as you write thousands of lines of code for projects in nine different languages, from low-level to high. Your portfolio will prove your skills and knowledge to everyone and anyone --- including you. You'll not only know how to code remotely on any computer in the world, but how *digital* computers and languages actually work and *why* people *really* need these things. After *BBoost* you'll be able to learn *anything* on your own --- especially coding languages and Linux distros --- because you just did. You'll destroy [imposter syndrome](/imposter/) and armor yourself against [Dunning-Krueger](/dk/) making you an ideal find for *any* occupation. What comes next is up to you. 


:::co-quote
[*The powerful play goes on and you may contribute a verse. 
What will your verse be?*]{.small}
:::

[**What does it cost?**]{.bigger}  
Nothing. No seriously, you only pay for the cost of the books you choose to buy (most of which are free under Creative Commons, [like this app](/copyright/)). This project is funded entirely by generous donations and run by a great community of veteran and beginner volunteers with no formal organization --- but *lots* of formal experience. You could say [RWX](/rwx/) is an open *knowledge* source project. One that seeks to crumble crusty cronyism and dispatch the stinking dogma that has suffocated *real* learning for far too long.

**Next Course:**  
May the 4th  
<span class=smallest>(But you can join us any time.)</span>

[**WIP WARNING:** This is currently a work in progress. Note the eventual absence of this warning to know when a first initial version has been released.]{.smallest}

</div>

<style>

  main {
    max-width: unset;
  }
  
  main.content {
    text-align: center;
    margin: 0;
  }

  .boxpoint {
    font-family: var(--sans-font);
    font-size: 1.4em;
    text-align: center;
    line-height: 1.5em;
    max-width: 600px;
    display: inline-block;
    margin-top: 2em;
  }

  .biggest { font-size: 1.3em; }
  .bigger { font-size: 1.15em; }
  .big { font-size: 1.02em; }
  .small { font-size: .9em; }
  .smaller { font-size: .8em; }
  .smallest { font-size: .7em;line-height:0.8; }

  footer {
    display: none;
  }

  a {
    color: none !important;
    text-decoration: none;
    background: none !important;
  }

  a:hover {
    text-decoration: dotted;
    background: none !important;
  }

</style>

