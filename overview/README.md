---
Title: Course Overview
---

Here's what this course is about and how it's organized.

1. [Prerequisites](/prereqs/)
1. [Not Like Other Courses]
1. [Target Occupations]
1. [Weekly Topics]
1. [Current Calendar]
1. [Session Time Breakdown]
1. [Stats and Numbers]

## Not Like Other Courses

In many ways this course is exactly the opposite of courses you have had in the past:

* No assignments, only suggestions and collaboration
* No lessons, just explanation and discussion
* No you versus test or others, just you versus you
* No grades, only finished projects that *you* decide
* No teachers, just experienced people here to help
* No classmates, but a community of learners
* No jobs, just confidence and trust you can foster

If you are uncomfortable with this level of personal responsibility and expectation then this course might not be for you. Either way know that you are among friends, not competitors. We are *all* learning something and having something to help others learn. Some of us just got started earlier.

## Target Occupations

When deciding what topics to include the following target occupations have been considered:

* Senior Software Developer
* Cybersecurity Professional
* Full-Stack Web Developer 
* Linux Systems Administrator
* Site Reliability Engineer
* IoT Embedded Device Developer

:::co-contrib
Contributors please keep the above list in mind when considering ideas for additional content. If you cannot confidently answer yes to the question "Does this content apply to every one of these occupations?" then it likely doesn't belong.
:::

## Weekly Topics 

The course is primarily organized by weeks. Each week is divided into five sessions matching the days of the week, Monday through Friday. Questions and discussion about the weekly topic are always welcome even if they don't fit the specific topic of any given 40-minute *segment*. Segments are there to guide discussion, not limit it.

1. [Getting Started](/start/)
1. [Knowledge as Source](/knowledge/)
1. [Learning to Work and Learn](/learning/)
1. [Running Linux](/linux/)
1. [The Linux Command Line, I-III (Book)](/linux-cli/)
1. [Becoming a Terminal Master (Vim/TMUX/Lynx/SSH)](/terminal/)
1. [The Linux Command Line, IV Bash Scripts (Book)](/linux-cli/#chapter-24)
1. [Networking and the Internet](/networking/)
1. [Learning Web Design, I,V HTML/Images (Book)](/learning-web-design/)
1. [Learning Web Design, II CSS (Book)](/learning-web-design/)
1. [Eloquent JavaScript, Chapters 1-11 (Book)](/eloquent-javascript/)
1. [Eloquent JavaScript, Chapters 12-21 (Book)](/eloquent-javascript/)
1. [Head First C, Chapters 1-6 (Book)](/head-first-c/)
1. [Head First C, Chapters 7-12 (Book)](/head-first-c/#chapter-7)
1. [Head First Go, Chapters 1-7 (Book)](/head-first-go/)
1. [Head First Go, Chapters 8-16 (Book)](/head-first-go/#chapter-8)

:::co-fyi
Remember that you can work on things well in advance and reach out to your mentor or community for help as you need it. This weekly schedule simply helps guide the in-session discussion to keep it on topic.
:::

## Current Calendar

              May 2020                 June 2020      
        Su Mo Tu We Th Fr Sa      Su Mo Tu We Th Fr Sa
                               #5     1  2  3  4  5  6
     #1     4  5  6  7  8  9   #6  7  8  9 10 11 12 13
     #2 10 11 12 13 14 15 16   #7 14 15 16 17 18 19 20
     #3 17 18 19 20 21 22 23   #8 21 22 23 24 25 26 27
     #4 24 25 26 27 28 29 30   #9 28 29 30
     #5 31
               July                 August       
        Su Mo Tu We Th Fr Sa      Su Mo Tu We Th Fr Sa
     #9           1  2  3  4  #13                    1
    #10  5  6  7  8  9 10 11  #14  2  3  4  5  6  7  8
    #11 12 13 14 15 16 17 18  #15  9 10 11 12 13 14 15
    #12 19 20 21 22 23 24 25  #16 16 17 18 19 20 21 22
    #13 26 27 28 29 30 31

## Session Time Breakdown

Each 3-hour session is divided into 40 minute flipped discussion segments dedicated to topical questions and answers and interspersed with 15-minute breaks. Each segment ends five minutes before the hour and starts ten minutes after the next:

-------- ------------------------
 11:00    Welcome
 11:10    **Segment One**
 11:55    Break
 12:10    **Segment Two**
 12:55    Break.
 01:10    **Segment Three**
 02:00    End
-------- ------------------------

Table: Session Time Breakdown

## Stats and Numbers

* 16 weeks
* 5 sessions/week
* 3 hours/session
* 15 total session hours/week
* 25 extra focus hours/week
* 40 total hours/week
* 40 minutes/segment
* 3 segments/session
* 80 total sessions
* 32 weekend days
* 112 total days
* 240 total segments 
* 3000+ pages of reading
* 640+ total hours
