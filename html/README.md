---
Title: Hyper Text Markup Language / HTML
Subtitle: The *Content* of the Web
---

*HTML* is the [declarative](/declarative/) [programming](/programming/) [language](/language/) for the [web](/web/). Every web [site](/site/) has some HTML in it. HTML is one of the most important coding language anyone will learn because it is so [ubiquitous](/ubiquitous/). Every single technical occupation requires some knowledge of HTML.
