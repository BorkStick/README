---
Title: Dunning-Kruger Effect
---

The *Dunning-Kruger Effect* generally refers to not knowing that you don't know. Or worse, being convinced you know something you actually have no clue about. Put another way, it is a [cognitive bias](/bias/) that leads to people of low ability thinking they are superior without realizing they're not. 

The opposite of Dunning-Kruger is [impostor syndrome](/impostor/), which is much more benign than DK.

## Quotes

> One of the painful things about our time is that those who feel certainty are stupid, and those with any imagination and understanding are filled with doubt and indecision. (Bertrand Russell)

> Real knowledge is to know the extent of one's ignorance. (Confucius)

> The fool doth think he is wise, but the wise man knows himself to be a fool. (Shakespeare)

> The Enemies of Truth. — Convictions are more dangerous enemies of truth than lies. (Nietzsche)

## Resources

* [I Suck. I Rock!](https://youtu.be/cFIF46j59LY)
* [The Dunning-Kruger Effect - Cognitive Bias - Why Incompetent People Think They Are Competent](https://youtu.be/y50i1bI2uN4)
* [DuckDuckGo Search](https://duckduckgo.com/?q=dunning kruger effect)
