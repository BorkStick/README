# Contributing to `rwx.gg`

## Writing Style, Grammar, and Usage

The `rwx.gg` project strictly follows [The Gregg Reference Manual](/gregg-reference-manual) for all written content.

