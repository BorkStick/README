---
Title: Donate
---

Currently the only way to donate is to subscribe to [rwxrob.live](https://rwxrob.live) or send money [directly](https://paypal.me/rwxrob).

