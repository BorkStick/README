---
Title: Getting Started
---

This week is for getting to know each other, getting used to how the course works, and setting yourself up with the apps and services to do everything else to follow.

## Day 1

1. Get to Know Each Other
1. Course [Overview](/overview/) and [Goals](/goals/)
1. [Open Credentials](/open-credential/)
1. [Course Logistics](/logistics/) and [Rules](/rules/)
1. Peek at [Learning to Learn](/learning/)
1. Understand Web Browsers
1. [Set Up Protonmail](/protonmail/)
1. Set Up [Discord](/discord/)
1. Set Up [Brave](https://brave.com) or [Chromium](https://www.chromium.org)
1. Play in [REPL.it](https://repl.it) Coding Sandbox

## Day 2

1. Get a Linux Terminal
    1. Install [WSL](/wsl/) on Windows 10
    1. Open `Terminal` on Mac
    1. Open `Terminal` on Linux
    1. Use *Shell* on [PicoCTF](https://picoctf.com)
1. Do Basic [Terminal](/terminal/) Stuff
1. Peek at [Bash Shell Scripting](/bash/)
1. Peek at [Vim Editor](/vim/)
1. [Generate Secure Shell Keys](/ssh-keygen/)
1. Better Ways to Use [Keyboard](/typing/) and [Mouse](/mouse/)
1. Set Up [GitLab](/gitlab/) and [GitHub](/github/)
1. Learn GitLab Editor
1. Create a Codebook
1. Publish Codebook to Netlify
1. Peek at [Web Development](/web/)
1. Publish First JAMStack Web Page

## Day 3


## Day 4

1. Personal Privacy and Safety Online
1. Drop Gmail and Google for Search
1. Set Up Duck.com
1. Set Up KeePassXC

## Day 5


