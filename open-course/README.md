---
Title: Open Course
---

An *open course* is one that you can learn from, duplicate, modify, remix, extend however you see fit --- even sell your version of the content or course so long as you provide full legal attribution to this original content in any derivative work and release your work under the same terms. All course content --- the writing, videos, code, exercises, and projects --- is [Creative Commons Attribution-ShareAlike 4.0 International license](https://creativecommons.org/licenses/by-sa/4.0/). 



