---
Title: Essential Services, Apps and Setup
---

Essential services are those online tools you absolutely need to perform as a technologist today:

---------------------  ------------------------
             Category  Suggested
---------------------  ------------------------
Graphic Web Browser    Brave or Chromium

Passwords              KeePassXC

Graphic Editor         VSCodium

Text Web Browser       Lynx

Virtual Machines       VirtualBox or VMware 

Terminal Editor        Vi (Vim)

IRC                    HexChat or WeeChat

Knowledge Source       Pandoc

Terminal Shell         Bash 4+

SpreadSheets           LibreOffice

Presentations          LibreOffice

Raster Images          Glimpse

Vector Images          Inkscape
---------------------  ------------------------

Table: Essential Apps

---------------------  ------------------
             Category  Suggested
---------------------  -------------------
Email                  ProtonMail.com

Rich Chat              Discord.gg

Dev Collab             GitHub.com

Source Hosting         GitLab.com

Web Hosting            Netlify.com

DNS Hosting            NameCheap.com

Cloud Servers          Linode.com or DigitalOcean.com

Twitter                Twitter.com

Linkedin               LinkedIn.com

VPN                    ProtonVPN.com

Diagrams               Draw.io
---------------------  -------------------

Table: Essential Services

## Justification

These selections are based on what meets the demand best based on the requirements of the industry, not personal preferences. For example, Chrome and Chromium based web browsers are now the standard and possess the most useful web application developer tools --- especially when dealing with web site performance analysis and progressive web app service worker development.

