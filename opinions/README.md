---
Title: Strong Opinions, Weakly Held
---

*Strong opinions, weakly held* summarizes the popular approach amongst technologists to hold very strong opinions of things that they have spent a long time researching, but being able to quickly and easily give up those opinions immediately and without remorse when presented with new objective information. This principle is the opposite of dogma where people become so invested in a belief or opinion that their very identify hinges on it being correct. There is no place for dogma in the world, let alone the tech world.
