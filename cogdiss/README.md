---
Title: Cognitive Dissonance
---

The state of being when a person freaks out because most or all of their reality turns out to be fake. If intense enough people can literally go crazy from it. As a mental defense mechanism people will often justify what they think is real without even realizing it. Cognitive dissonance is often associated with [Dunning-Kruger Effect](/dk/).
