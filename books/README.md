---
Title: Essentials Books
Subtitle: Get Started Right
---

Everyone learns differently but actively working through a good, *modern* book from a reliable author and publisher remains the best way for most to obtain a *comprehensive* understanding of the concepts. In fact, this truth has been humorously codified in tech culture with as RTFM, "read the freaking manual." When combined with *practical* application of the things learned on projects, challenges, and exercises --- perhaps of your own creation --- and a mentor or community to help you when you get stuck, books provide the foundation of everything you need to learn independently.

## Beginner Books

When combined with other fundamentals, these four books provide a solid base tech proficiency:

1. [The Linux Command Line, 2nd Edition (5th Internet Edition)](TLCL-19.01.pdf) (free)
1. [Learning Web Design, 5th Edition](https://www.amazon.com/Learning-Web-Design-Beginners-JavaScript/dp/1491960205/) (not free)
1. [Eloquent JavaScript](Eloquent_JavaScript.pdf) (free)
1. [Head First C](https://www.amazon.com/Head-First-C-Brain-Friendly-Guide/dp/1449399916) (not free)

These books aren't perfect. Sometimes they completely miss an important concept or add something that is outdated or irrelevant. But overall they seem to cover the best basics for beginners. You might find you prefer other, denser books out there as well.

::: co-fyi
Incidently, learning web tech (HTML, CSS, JavaScript, DOM, HTTP) is required to become a cybersecurity professional since the web is second only to phishing scams as a hacker attack vector. This is why WGU requires certification as a "Web Site Developer" for their [Cybersecurity and Information Assurance Bachelor's degree program](https://www.wgu.edu/online-it-degrees/cybersecurity-information-assurance-bachelors-program.html)). It goes without saying that deep web skills are also required to win most bug bounties.
:::

![The Linux Command Line](the-linux-command-line.jpg)

![Learning Web Design](learning-web-design.jpg)

![Eloquent JavaScript](eloquent-javascript.jpg)

![Head First C](head-first-c.jpg)

::: co-warning
If you wish to use Node for JavaScript and want the latest be sure to use [this installer script](https://gitlab.com/rwxrob/config/-/blob/master/installers/install-node) since all others on the Internet dangerously ignore the step of checksum validation to avoid man-in-the-middle attacks. Have someone you trust validate this installer as well to confirm it is secure.
:::

## What about *Learning JavaScript*?

[Learning JavaScript, 3rd Edition](https://www.amazon.com/Learning-JavaScript-Essentials-Application-Development/dp/1491914912/) is a *solid* book that covers just the essentials of modern JavaScript in a wonderful way. But there are a few specific reasons it is no longer include it in the list of beginner books:

1. It's not free.
1. It can't be updated.
1. It doesn't cover HTTP.

*Eloquent JavaScript* has its own challenges --- specifically the introduction of high-end mathematics not suitable for a 12-year-old learning on their own --- but it *can be changed and improved* while *Learning JavaScript* cannot. That is the beauty of open content licensed under [Creative Commons](/creative-commons/).

## Where did *Head First Go* go?

[Head First Go](https://www.amazon.com/Head-First-Go-Jay-McGavren-dp-1491969555/dp/1491969555/) is the *only* beginner book on Go currently but has been removed from the list for the following reasons:

1. It's *really* out of date.
1. It can't be updated.
1. It's not free.

## Aren't tech books bad because they get out of date?

*Absolutely.* But the advantages of having a common author, with a common writer's voice and exceptional knowledge of that material is worth it over the alternatives.

Of these books Head First Go is the most out of date, but covers the core language concepts enough to become productive with it immediately.

## What about videos (Udemy, YouTube, etc.)?

Videos are a great way to *supplement* your comprehensive learning from books bolstered with projects and exercises, but videos are rarely anywhere nearly as comprehensive as is required to truly master the topic. Videos are also nearly impossible to search by keyword or index --- especially when digital books are taken into consideration.

## WARNING: Books to Avoid

Here's a list books that for one reason or another you should really consider avoiding all together. Some are simply outdated information, others are just dead wrong, and some are written by author's who *lack actual work experience* and any rhetorical writing skill.

### *Learning Bash*

Learning Bash from O'Reilly is far too outdated (covering only Bash 3) to be worth buying. Besides you can learn everything in it for free from the listed alternative.

### *You Don't Know JavaScript*

The click-bait title says everything you need to know about this author. It is not surprising that this book is *full* of factual and practical errors and definitely *not* for beginners. When and if you want to waste your free-time exploring the nooks and rotten crannies of a language that has moved far beyond where it was even five years ago then fine, otherwise save yourself the wasted time and skip this horrible book (and author).

