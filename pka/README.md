---
Title: Progressive Knowledge Apps
---

A *progressive knowledge app* is a knowledge base (usually written in [Markdown](/markdown/) turned into a [progressive web app](/pwa/). Since the [knowledge base](/knowledge-base/) source is written in Markdown it can be rendering in any number of consumable media formats including ePub, Kindle, PDF, and others. Modern web browsers support service workers allowing PKAs to contain their own localized search engines without need for an Internet connection at all once they've been downloaded.

[Minimum Viable Knowledge Base](/mvkb/){.see}

