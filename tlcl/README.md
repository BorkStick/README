---
Title: The Linux Command Line (Annotations)
---

The freely distributable book [The Linux Command Line](the-linux-command-line-2nd-ed.pdf) is currently the most comprehensive and up-to-date introduction to the Linux Bash command line. It is the only book in print that covers Bash 4+ associative arrays, for example. William Shotts is a great writer with an easy to read style. The following annotations are designed to clarify and update the reader when needed (as well as to point out the rare errors that occur in the book).

## Introduction

Be sure you read all of this.

## Chapter 1

TODO

## Chapter 24: Writing Your First Script {#chapter-24}

Finally we get to write some shell code. 

:::co-fyi
Don't confuse the term [shell code](/shell-code/) with *shell script*. 

The first is a term from [pentesting](/pentesting/) meaning it is code that runs on a system to give you shell access to that system, which usually is *not* a full shell like Bash.

The second is the common way to refer to the scripts you will be writing in this part of the book.
:::

### Bash is *Usually* the Default Command Line

Bash has been the default shell for Linux for several decades but was replaced as the *system startup* scripting language by Dash on most Linux distributions some time ago. But Bash remains the default interactive shell assigned to new users.

#### What about Zsh?

Don't use it. No seriously.

#### What about Dash?

`/bin/sh` is [symbolically linked](/symlink/) `/bin/dash`, a light-weight, [POSIX-compliant](/posix/) shell that runs much more quickly for use in the Linux startup process. 

Despite the claim on Dash's home page that it is *the* Linux shell. The [Arch distro](/arch/) symlinks to `/bin/bash` instead.

### Bash History

Bash code is unlike most other coding languages because it evolved from Bourne Shell which was released in 1979. 

Bourne *Again* Shell has added stuff from lots of shells since that time including primarily Korn Shell, which came out in 1983. The first version of Bash was released in 1989. And the latest significant Bash release (version 5.0) came out on [January, 2019](https://lwn.net/Articles/776223/). More significantly, however, was the release of Bash 4.0, which came out in February, 2009 and included [associative arrays](/hashmaps/), which are covered by the 5th edition of this book.

:::co-warning
This book is the *only* book available anywhere that covers Bash 4+ associative arrays, which is why it was picked even over Learning Bash from O'Reilly.
:::

### Bash is Interpreted

This chapter jumps right into how to use Bash but it is worth understanding that Bash is an [interpreted](/interpreter/) language and that the [shebang](/shebang/) line tells the [operating system](/os/) to send the script file to the `/bin/bash` binary command which interprets it into [system calls](/system-calls/) that `bash` itself executes. There is no intermediary [bytecode](/bytecode/) created.

### `PATH` Environment Variable

These days `~/.local/bin` is the preferred place to hide your local `bin` directory --- usually by symlinking to a directory in your personal [dotfiles config](/dotfiles/). 

You might consider adding the following script to your `~/.local/bin/` directory or adding the echo line as an alias in `~/.bashrc` file in order to simplify reading your PATH.

Script Version

```bash
#!/bin/bash
echo -e ${PATH//:/\\n}
```

Bash Alias

```bash
alias path="echo -e ${PATH//:/\\n}"
```
