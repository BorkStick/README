---
Title: Running Linux
---

## Day 1

1. What is Linux?
1. Overview of Distros
1. Which Linux should *I* use?
1. What about virtual machines?
1. What about containers?


The easiest, fastest, and safest way to get a true Linux experience is currently to install PopOS from System76. You may prefer Linux Mint Cinnamon, another good beginner distribution, or EndeavorOS an Arch-based alternative for beginners. Fedora is a good option for beginners who specifically want to build skills compatible with the RedHat ecosystem (mostly enterprise organizations).

* <https://support.system76.com/articles/install-in-vm/>

## What about Manjaro?

Manjaro is generally a very bad beginning distribution for many reasons chief of which is its relative (and surprising) instability. It may seem good a start but the stories of people having serious problems are well documented. In one case clicking the update button bricked thousands of people's machines and Manjaro's official response was *no* response leaving the forums filled with frustrated beginners who were being condescendingly told they should have run the upgrade from pseudo-tty instead of clicking "Update". Manjaro as an organization is also downright unethical in many of its corporate practices and has repeatedly demonstrated its focus on profit above the needs of its users and community.

Obviously people will make their own decisions from their own experiences, but there are *a lot* of blogs and videos from people echoing these experiences and conclusions.
