---
Title: Brackets
Subtitle: Square, Curly, Angle
---

The term *brackets* can be confusing. Here's a table to remind you:

-------- -----------------------------
 `[]`     Square brackets 
 `{}`     Curly brackets (or braces)
 `<>`     Angle brackets
-------- -----------------------------

