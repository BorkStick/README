---
Title: Prerequisites
---

This course requires that you be able to read, write, and do math generally required of a teenager in most developed countries:

* Be 13 or older (required by law), 16+ suggested
* Have a computer with a keyboard and a mouse
* Have a compatible computer operating system
    * Windows 10 or above
    * Mac Sierra or above
    * Any Linux
    * No Chromebooks
* Able to use a computer and graphical web browser
* Type 30 words per minute from home row
* Perform Algebraic operators and write functions
* Read Harry Potter and middle-school text books

