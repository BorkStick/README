---
Title: Knowledge Base
---

A *knowledge base* is simply a collection of structured and unstructured information in a way that can be consumed, shared, and queried. This [knowledge-app](pka) is built on a [rotating release](/rotating-release/) knowledge base.
