---
Title: Declarative
---

A *declarative* [language](/language/) is one in which you write what you want rather then the steps to get what you want. You *declare* the outcome you desire. [HTML](/html/), [PostScript](/postscript/), and [SQL](/sql/) are all declarative languages.

> 💢 People including Harvard CS50 say that HTML is not a “programming” language because its not like the other [procedural](/procedural/) languages they are familiar with, but it is. It *so* is. Harvard is dead wrong. But don't take my word for it, do 15 minutes of research and you will see what I mean.
