---
Title: Markup Language
---

Markup is writing that is marked up with formatting symbols and syntax. [HTML](/html/) and [Markdown](/markdown/) are examples of markup languages.
