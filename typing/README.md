---
Title: Typing
Subtitle: Keyboards, Typing Code
---

You can't do anything these days without being able to use a keyboard. The selection of a keyboard, however, is a *very* personal decision.

## Discussion Questions

* What is the minimum typing speed to work in technology?
* Do I *have* to learn touch typing?
* Should I customize my keyboard?
* What is the best keyboard?
* What about Dvorak?

## Resources

* <https://typing.com>
* <https://zty.pe>
* <https://nitrotype.com>
* <https://www.ostechnix.com/wpm-measure-typing-speed-terminal/>
