<!doctype html>
<html lang="en">
<head>
    <title>Markdown | RWX.GG</title>
    <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate"/>
  <meta http-equiv="Pragma" content="no-cache"/>
  <meta http-equiv="Expires" content="0"/>
  <!-- link rel="shortcut icon" href="/assets/img/logo.png" type="image/png"-->
  <link rel="preload" href="/assets/main.css" as="style">
  <link rel="stylesheet" href="/assets/main.css">
</head>
<body>
<header class=nav-bar>
  <nav>
    <div class=sidebar-button></div>
    <div id=bwtoggle></div>
    <a class="homelink spy" href="/">r<span class=hideable>wx.gg</span></a>
    <div class="nav-right">
      <!-- div id=search-box></div -->
      <ul class=nav-links>
        <li class=nav-item><a class=nav-link href="/overview/">Overview</a></li>
        <li class=nav-item><a class=nav-link href="/faq/">FAQ</a></li>
        <li class=nav-item><a class=nav-link href="/join/">Join</a></li>
        <li class=nav-item><a class=nav-link href="/donate/">Donate</a></li>
      </ul>
    </div>
  </nav>
</header>
<main class=content>
<a id=top></a>
<h1>Markdown</h1>
<h2>Invented by Writers, Not Scientists</h2>
<p>Markdown is a simple <a href="/markup/">markup</a> language invented by writers who didn’t want to write <a href="/html/">HTML</a>. Markdown has become the standard for most technical writing and is supported on Reddit, StackExchange, Discord, GitLab, GitHub.</p>
<h2 id="standard-knowledge-source-language">Standard Knowledge Source Language</h2>
<p>Markdown has become the closest thing to a universal standard for writing all documentation today. In addition, authors write entire textbooks and novels in Markdown these days. When combined with a tool like <a href="/pandoc/">Pandoc</a> your Markdown can be converted to <em>every other format on the planet</em>. Markdown is the <em>only</em> markup writing language that can claim this. Therefore, <em>everyone</em> should learn it from those needing a solid, sustainable way to take notes, to those creating entire <a href="/knowledge-base/">knowledge bases</a>. Ironically Markdown is practically not taught at all today in any educational setting.</p>
<p><a href="/wikis/" class="see">What about wikis?</a></p>
<h2 id="types-of-markdown">Types of Markdown</h2>
<p>Eventually you’ll want to use <a href="/pandoc-markdown/">Pandoc Markdown</a> for everything. (In fact, <a href="https://gitlab.com/rwx.gg/README/-/tree/master/markdown">this document</a> is written in it.) Pandoc Markdown is far and away the most powerful, sustainable, and supported format for capturing knowledge source, which is why the R language project adopted it as their language documentation format.</p>
<p>However, you can learn basic Pandoc Markdown in less than 30 minutes and it will work <em>everywhere</em>.</p>
<p>Why the differences?</p>
<p>The original Markdown was never standardized and has evolved into more than a dozen different flavors many of which are incompatible with one another. Thankfully there are only three versions that really matter:</p>
<table>
<tbody>
<tr class="odd">
<td style="text-align: center;"><a href="#basic-markdown">Basic Markdown</a></td>
<td style="text-align: center;">Emphasis on simplicity and compatibility.</td>
</tr>
<tr class="even">
<td style="text-align: center;"><a href="/commonmark/">CommonMark</a></td>
<td style="text-align: center;">General industry standard (but no tables).</td>
</tr>
<tr class="odd">
<td style="text-align: center;"><a href="/pandoc-markdown/">Pandoc Markdown</a></td>
<td style="text-align: center;">Maximum power and flexibility, no HTML deps.</td>
</tr>
</tbody>
</table>
<h2 id="basic-markdown">Basic Markdown</h2>
<p>Here is quick overview that you can complete in 20 minutes covering the most basic, compatible Markdown everyone should learn. The priority is on simplicity and maximum compatibility. It allows your content to be used <em>everywhere</em>. There is no more ubiquitous form of knowledge storage.</p>
<h3 id="paragraphs">Paragraphs</h3>
<p>Paragraphs are just long, single lines that are followed by a blank line. That way they wrap properly no matter what editor you are using or its width. Put one blank line after each paragraph to separate it from the rest.</p>
<p>Paragraphs can contain formatting.</p>
<figure>
<img src="good-wrap.png" alt="" /><figcaption>Example of Good Wrapping</figcaption>
</figure>
<p>Although every Markdown tool allows multiple lines to be grouped into a single paragraph so long as a they are followed by a blank line this practice is rather bad when editing those same files with editor or terminal or <a href="/tmux/">tmux</a> pane widths that are less than the line length. They are completely unreadable. Stay safe, just use single long lines. All modern (and even most ancient editors) support auto-wrapping the line in the terminal rather than effectively hard wrapping it in the display.</p>
<figure>
<img src="bad-wrap.png" alt="" /><figcaption>Example of Bad Wrapping</figcaption>
</figure>
<h3 id="formatting">Formatting</h3>
<p>One star for <em>italics</em>:</p>
<div class="sourceCode" id="cb1"><pre class="sourceCode markdown"><code class="sourceCode markdown"><span id="cb1-1"><a href="#cb1-1"></a>one star for *italics* </span></code></pre></div>
<p>Two stars for <strong>bold</strong>:</p>
<div class="sourceCode" id="cb2"><pre class="sourceCode markdown"><code class="sourceCode markdown"><span id="cb2-1"><a href="#cb2-1"></a>two stars for **bold**</span></code></pre></div>
<p>Three stars for <strong><em>bold italics</em></strong>:</p>
<div class="sourceCode" id="cb3"><pre class="sourceCode markdown"><code class="sourceCode markdown"><span id="cb3-1"><a href="#cb3-1"></a>three stars for ***bold italics***</span></code></pre></div>
<p>Backticks for <code>code</code> also known as <code>monospaced</code>:</p>
<div class="sourceCode" id="cb4"><pre class="sourceCode markdown"><code class="sourceCode markdown"><span id="cb4-1"><a href="#cb4-1"></a>backticks for <span class="in">`monospaced`</span></span></code></pre></div>
<div class="co-warning">
<p>Do not use underscore (_) ever. Yes the original Markdown supported it, but it was always a bad idea. Eventually it will bite you. It is not widely supported and causes problems when used for actual names.</p>
<p>Also, avoid combine or overlap formatting as much as possible and avoid formatting a portion of a single word, which is not handled consistently — especially by syntax-highlighting tools and editors.</p>
</div>
<h3 id="headings">Headings</h3>
<p>Headings (often incorrectly called headers) begin with 1-6 hashtags (<code>#</code>) followed by a space and then the title text followed by a single blank line.</p>
<div class="sourceCode" id="cb5"><pre class="sourceCode markdown"><code class="sourceCode markdown"><span id="cb5-1"><a href="#cb5-1"></a><span class="fu"># Level One</span></span>
<span id="cb5-2"><a href="#cb5-2"></a></span>
<span id="cb5-3"><a href="#cb5-3"></a>Paragraphs and such here.</span>
<span id="cb5-4"><a href="#cb5-4"></a></span>
<span id="cb5-5"><a href="#cb5-5"></a><span class="fu">## Level Two</span></span>
<span id="cb5-6"><a href="#cb5-6"></a></span>
<span id="cb5-7"><a href="#cb5-7"></a>Paragraphs and such here.</span></code></pre></div>
<p>Formatting is allowed in headings but can be problematic with some renderers. Avoid if you can.</p>
<div class="co-tip">
<p>Generally you should never have more than one first level heading (<code># Heading One</code>) because search engines prioritize it. When using Pandoc you will not even need a level one heading because the <code>Title</code> is better placed in the meta-data property instead and rendered with the Pandoc Template.</p>
</div>
<h3 id="links-hyperlinks">Links / Hyperlinks</h3>
<p>Hyperlinks (stuff you click on) come in three basic forms: words, URLs, and images.</p>
<h4 id="hyperlinked-words">Hyperlinked Words</h4>
<p>The most common link in Markdown is just <a href="https://rwx.gg">words you can click on</a>. The web address must be either pointing to a remote site or so something on the same site that document is on.</p>
<div class="sourceCode" id="cb6"><pre class="sourceCode markdown"><code class="sourceCode markdown"><span id="cb6-1"><a href="#cb6-1"></a>Here is a <span class="co">[</span><span class="ot">link to rwx.gg</span><span class="co">](https://rwx.gg)</span>.</span></code></pre></div>
<div class="co-warning">
<p>Never use “reference links” that allow you to put the actual link elsewhere in the document (usually at the bottom). While this made sense when Markdown was invented and the emphasis was on how pretty and readable the Markdown source text itself was this emphasis has changed today. Separating your link several hundred lines away from the text that is linked is <em>really</em> bad practice because it decouples the link making it much harder to copy and paste to another document — or worse — you will edit your document and completely forget to remove them leaving them orphaned.</p>
</div>
<h4 id="autolinked-urls">Autolinked URLs</h4>
<p>Sometimes you want the web address to actually appear like <a href="https://rwx.gg/markdown" class="uri">https://rwx.gg/markdown</a>.</p>
<div class="sourceCode" id="cb7"><pre class="sourceCode markdown"><code class="sourceCode markdown"><span id="cb7-1"><a href="#cb7-1"></a>Here is URL to <span class="ot">&lt;https://rwx.gg/markdown&gt;</span> that will appear in full.</span></code></pre></div>
<div class="co-warning">
<p>Never trust that your external links will be detected and autolinked automatically just because they start with <code>http</code>. There are still Markdown engines out there that do not autodetect them (for good reason). This is one of the dumbest things GitHub added to their flavor of Markdown.</p>
</div>
<p>This also works with other link types besides <code>http</code>. (Yes, there are several other URL schemas.)</p>
<div class="sourceCode" id="cb8"><pre class="sourceCode markdown"><code class="sourceCode markdown"><span id="cb8-1"><a href="#cb8-1"></a>Mail me at <span class="ot">&lt;mailto:rwx@robs.io&gt;</span>.  </span>
<span id="cb8-2"><a href="#cb8-2"></a>Phone me at <span class="kw">&lt;tel:555-555-5555&gt;</span></span></code></pre></div>
<h3 id="images">Images</h3>
<p>Images are just links with an exclamation point in front. Make sure to put a blank line before and after any image for maximum compatibility. Inline images are not widely supported and mess up other formatting in almost all cases.</p>
<pre><code>
![gnome](/assets/img/mr-rob-gnome.png)
</code></pre>
<p>Remote image (but usually bad practice):</p>
<p><img src="https://rwx.gg/assets/img/mr-rob-gnome.png" /></p>
<p>Images can also be used as links.</p>
<pre><code>[![skilstak logo](https://skilstak.io/copyright/slogobox.png)](https://skilstak.io)
</code></pre>
<p><a href="https://robs.io"><img src="https://rwx.gg/assets/img/mr-rob-gnome.png" alt="gnome" /></a></p>
<h3 id="lists">Lists</h3>
<p>Simple lists are supported by pretty much everything. Put the list items one to a line. Make sure to put a blank line after the list.</p>
<p>Use stars followed by spaces (<code>*</code>) for bulleted (unordered) lists:</p>
<div class="sourceCode" id="cb11"><pre class="sourceCode markdown"><code class="sourceCode markdown"><span id="cb11-1"><a href="#cb11-1"></a><span class="ss">* </span>some</span>
<span id="cb11-2"><a href="#cb11-2"></a><span class="ss">* </span>thing</span>
<span id="cb11-3"><a href="#cb11-3"></a><span class="ss">* </span>here</span></code></pre></div>
<ul>
<li>some</li>
<li>thing</li>
<li>here</li>
</ul>
<p>Use the number one followed by a period and a space (<code>1.</code>) for numbered (ordered) lists:</p>
<div class="sourceCode" id="cb12"><pre class="sourceCode markdown"><code class="sourceCode markdown"><span id="cb12-1"><a href="#cb12-1"></a><span class="ss">1. </span>HTML </span>
<span id="cb12-2"><a href="#cb12-2"></a><span class="ss">1. </span>CSS</span>
<span id="cb12-3"><a href="#cb12-3"></a><span class="ss">1. </span>JavaScript</span>
<span id="cb12-4"><a href="#cb12-4"></a><span class="ss">1. </span>Go</span>
<span id="cb12-5"><a href="#cb12-5"></a><span class="ss">1. </span>Bash</span></code></pre></div>
<ol type="1">
<li>HTML</li>
<li>CSS</li>
<li>JavaScript</li>
<li>Go</li>
<li>Bash</li>
</ol>
<p>Always use <code>1.</code> so that if you change the order you do not have to renumber the source itself. It will automatically change the number order when rendered.</p>
<h3 id="separators">Separators</h3>
<p>Also called “horizontal rule.” These just break up the page usually with a horizontal line.</p>
<div class="sourceCode" id="cb13"><pre class="sourceCode markdown"><code class="sourceCode markdown"><span id="cb13-1"><a href="#cb13-1"></a>----</span></code></pre></div>
<hr />
<p>Use four dashes for consistency even though there are dozens of ways to indicate separation (some of which allow stars to be used as well). This consistency allows you to easily find your separators and keeps them from being confused with YAML markers (which use three dashes) and inline formatting (which uses stars <code>*</code>).</p>
<h3 id="hard-returns">Hard Returns</h3>
<p>Hard returns are a way of starting a new line within a given paragraph. Type two spaces (<code>␣␣</code>) followed by the line return.</p>
<div class="sourceCode" id="cb14"><pre class="sourceCode markdown"><code class="sourceCode markdown"><span id="cb14-1"><a href="#cb14-1"></a>Roses are red␣␣</span>
<span id="cb14-2"><a href="#cb14-2"></a>Violets are blue</span></code></pre></div>
<p>Rose are red<br />
Violets are blue</p>
<div class="co-tips">
<p>The <a href="/vim#plugins/">Pandoc VIM plugin</a> is particular useful at showing these with <a href="/ligature/">ligatures</a>.</p>
</div>
<p><img src="pandoc-hb.png" /></p>
<h3 id="blocks">Blocks</h3>
<p>Blocks separate text or code from the document usually as a box. There are two main block types to remember: <em>plain</em> (preformatted, as-is) blocks and <em>code fences</em>. Both use three <a href="/backtick/">backticks</a> to “fence off” the text or code.</p>
<h4 id="plain">Plain</h4>
<p>When you just want the text to appear exactly as it is just use the triple-backtick fence posts.</p>
<div class="sourceCode" id="cb15"><pre class="sourceCode markdown"><code class="sourceCode markdown"><span id="cb15-1"><a href="#cb15-1"></a><span class="in">```</span></span>
<span id="cb15-2"><a href="#cb15-2"></a><span class="in">    Roses are red</span></span>
<span id="cb15-3"><a href="#cb15-3"></a><span class="in">    Violets are violet</span></span>
<span id="cb15-4"><a href="#cb15-4"></a><span class="in">```</span></span></code></pre></div>
<pre><code>    Roses are red
    Violets are violet</code></pre>
<h4 id="code-fences">Code Fences</h4>
<p>Code blocks are perhaps the single biggest reason to use markdown for all your tech writing and note taking. Usually the code will automatically be syntax highlighted for you. This provides very high-quality publications very easily or just amazing personal logs and notes.</p>
<p>When you want to add syntax highlighting or otherwise indicate how the text should be handle provide an information tag immediately following the first triple-backtick fence, so for JavaScript it would be:</p>
<div class="sourceCode" id="cb17"><pre class="sourceCode markdown"><code class="sourceCode markdown"><span id="cb17-1"><a href="#cb17-1"></a><span class="in">```js</span></span>
<span id="cb17-2"><a href="#cb17-2"></a><span class="bu">console</span><span class="op">.</span><span class="fu">log</span>(<span class="st">&#39;hello world&#39;</span>)</span>
<span id="cb17-3"><a href="#cb17-3"></a><span class="in">```</span></span></code></pre></div>
<div class="sourceCode" id="cb18"><pre class="sourceCode js"><code class="sourceCode javascript"><span id="cb18-1"><a href="#cb18-1"></a><span class="bu">console</span><span class="op">.</span><span class="fu">log</span>(<span class="st">&#39;hello world&#39;</span>)</span></code></pre></div>
<div class="co-fyi">
<p>Note that I do not have color syntax highlighting active here because I find it distracts and doesn’t print well when printed copies of lessons are needed, which they are in many educational settings.)</p>
</div>
<p>Although there are other ways to write blocks, using triple-backtick fences is the most consistent way to do them all. This allows quickly finding your blocks when editing as well as filtering them out easily with scripting or simple parsing. It is also the most widely supported. Discord, for example, only supports this format of code fence.</p>
<p>Here is a short list of supported language tags:</p>
<table style="width:46%;">
<colgroup>
<col style="width: 15%" />
<col style="width: 30%" />
</colgroup>
<thead>
<tr class="header">
<th style="text-align: right;">Tag Lang</th>
<th style="text-align: left;">uage</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: right;">md json js html css sh</td>
<td style="text-align: left;">Markdown JSON JavaScript HTML CSS Shell or Bash</td>
</tr>
</tbody>
</table>
<h5 id="exception-for-markdown">Exception for Markdown</h5>
<p>In the single exceptional case where you need your block to contain markdown code you should use three or four tildes (<code>~~~markdown</code> or <code>~~~~markdown</code>). Again, this consistency allows you to filter out blocks from simple scripts that examine each line which can be useful for coding keyword searches and such.</p>
<pre><code>~~~markdown
Here is *some* markdown.

```js
console.log(&#39;hello example&#39;)
```
~~~</code></pre>
<div class="sourceCode" id="cb20"><pre class="sourceCode markdown"><code class="sourceCode markdown"><span id="cb20-1"><a href="#cb20-1"></a>Here is *some* markdown.</span>
<span id="cb20-2"><a href="#cb20-2"></a></span>
<span id="cb20-3"><a href="#cb20-3"></a><span class="in">```js</span></span>
<span id="cb20-4"><a href="#cb20-4"></a><span class="bu">console</span><span class="op">.</span><span class="fu">log</span>(<span class="st">&#39;hello example&#39;</span>)</span>
<span id="cb20-5"><a href="#cb20-5"></a><span class="in">```</span></span></code></pre></div>
<div class="co-mad">
<p>There are literally an infinite number of possible ways to indicate a block supported by the original and most derived Markdown parsers. Just stick with these two options. Consistency is far more important than artistic expression. Blocks are particularly important to keep consistent because you will frequently want to simply strip them out for keyword searches and such. Following these suggestions makes this trivial even from simple shell scripts.</p>
</div>
<div class="co-warning">
<p>Make sure there is no space after the backticks and before the block identifier (<code>js</code> in the example).</p>
</div>
<div class="co-fyi">
<p>Technically paragraphs, lists, and even separators are also considered blocks when parsed.</p>
</div>
<h3 id="blockquotes">Blockquotes</h3>
<p>Blockquotes are for quotations and <em>only</em> quotations. Avoid the temptation to use them for anything else because if you do you can semantically identify all the <em>actual</em> quotes in your content.</p>
<p>Begin each line of the block with a greater-than sign (right <a href="/brackets/">angle-bracket</a>).</p>
<p>Usually you will just have a single paragraph:</p>
<div class="sourceCode" id="cb21"><pre class="sourceCode markdown"><code class="sourceCode markdown"><span id="cb21-1"><a href="#cb21-1"></a><span class="at">&gt; &quot;One of the painful things about our time is that those who feel certainty are stupid, and those with any imagination and understanding are filled with doubt and indecision.&quot; (Bertrand Russell)</span></span></code></pre></div>
<blockquote>
<p>“One of the painful things about our time is that those who feel certainty are stupid, and those with any imagination and understanding are filled with doubt and indecision.” (Bertrand Russell)</p>
</blockquote>
<div class="co-tip">
<p>Use of quotation marks surrounding the text of the quote itself is completely up to you but is recommended so that multiple quotes can be combined next to one another without reader confusion.</p>
</div>
<p>In the rare case that your quotation expands beyond a single line make sure to join separate paragraphs with a blank line that is also included:</p>
<div class="sourceCode" id="cb22"><pre class="sourceCode markdown"><code class="sourceCode markdown"><span id="cb22-1"><a href="#cb22-1"></a><span class="at">&gt; This is the first part of the quote.</span></span>
<span id="cb22-2"><a href="#cb22-2"></a><span class="at">&gt;</span></span>
<span id="cb22-3"><a href="#cb22-3"></a><span class="at">&gt; Here is the second part.</span></span></code></pre></div>
<blockquote>
<p>This is the first part of the quote.</p>
<p>Here is the second part.</p>
</blockquote>
<h2 id="tables">Tables</h2>
<p>Just don’t use them. If you do need them, use <a href="/pandoc-markdown/">Pandoc Markdown</a> instead (not <a href="/gfm/">GitHub Flavored Markdown</a>). For <em>basic</em> Markdown it is more important to maintain compatibility and no one agrees on how tables should be done. It is one of the most hotly debated topics in the Markdown community.</p>
<div class="co-mad">
<p>Absolutely <em>do not use</em> GitHub’s brain-dead table format, which is so stupid people have actually written tools to generate them defeating the entire purpose of Markdown in the first place. GitHub tables are a GitHub addition and were <em>never</em> supported by Markdown and are not supported by CommonMark. So if you ever want to move to another Git hosting site (like GitLab, and you should) you will quickly discover your tables do not render. If and when you are ready to be okay graduating to a more powerful Markdown syntax always use Pandoc Markdown instead, which has much broader and deeper support and includes several much more powerful and simple table formats that GitHub’s.</p>
</div>
<h2 id="practice-challenges">Practice Challenges</h2>
<ol type="1">
<li><p>Create a <code>markdown.md</code> document on <a href="/gitlab/">GitLab</a> or <a href="/github/">GitHub</a> and add one of everything from the description of <a href="#basic-markdown">Basic Markdown</a>.</p></li>
<li><p>Start and maintain a personal log by creating a <code>log.md</code> file on <a href="/gitlab/">GitLab</a> or <a href="/github/">GitHub</a> with daily entries about things you have done or need to remember. Put the most recent entries at the top and use a level two heading for the date and/or time.</p></li>
<li><p>Start and maintain a simple public blog in Markdown and eventually learn to convert it to HTML with <a href="/pandoc/">pandoc</a> and publish it automatically with <a href="/gitlab/">GitLab</a> and <a href="/netlify/">Netlify</a>.</p></li>
<li><p>Try out all the <a href="#basic-markdown">Basic Markdown</a> markup on Discord and/or Slack and see how much is supported.</p></li>
<li><p>Read and optionally try out <a href="#basic-markdown">Basic Markdown</a> on Reddit or StackExchange.</p></li>
</ol>
<h2 id="resources">Resources</h2>
<ul>
<li><a href="https://commonmark.org">CommonMark Specification</a></li>
<li><a href="https://pandoc.org/MANUAL.html#pandocs-markdown">Pandoc Markdown Specification</a></li>
</ul>
</main>
<footer>
  <p><a href="/copyright/" id=copyright>© 2020 Rob Muhlestein. This written content is licensed under a Creative Commons Attribution-ShareAlike 4.0 International License. Any code is released into the public domain with no warranty.</a><br>See something wrong? <a href="https://gitlab.com/rwx.gg/README/-/issues">Open a ticket</a></p>
</footer>
<script src="/assets/main.js"></script>
</body>
</html>
